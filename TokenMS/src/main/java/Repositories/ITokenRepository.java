package Repositories;

import TokenCore.InvalidTokenException;
import TokenCore.Token;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;

/**
 * Created by karol on 18.1.2019.
 */
public interface ITokenRepository {
    boolean addTokens(Token token, String CPR);
    HashSet<Token> getCustomerTokens(String CPR);
    Token getToken(String barCode) throws InvalidTokenException;
    boolean invalidateToken(String barcode);
    boolean tokenExists(String barCode);
    boolean isTokenValid(String barcode);
    boolean clearDatabase();
}
