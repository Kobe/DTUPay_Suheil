package CustomerCore;

public class Customer {

    private String firstName;
    private String lastName;
    private String CPR;


    public Customer(){
        System.out.println("Customer init...");
    };

    public Customer(String firstName, String lastName,String CPR) throws InvalidCPRException {
        this.firstName = firstName;
        this.lastName = lastName;
        this.CPR = CPR;

        /*if(!isCPRValid(CPR)) {
            throw new InvalidCPRException("Invalid CPR Format: CPR Must be 10 Digits Long");
        }else {
            this.CPR = CPR;
        }*/
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        System.out.println("Setting last name...");
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getCPR() {
        return CPR;
    }

    public void setFirstName(String firstName) {
        System.out.println("Setting first name...");
        this.firstName = firstName;
    }

    public void setCPR(String CPR){
        System.out.println("Setting CPR...");
        this.CPR = CPR;
    }

    public boolean isCPRValid() {

        // Not a real CPR validator, because it is hard to test with real CPR number.
        // This will ensure that the CPR is comprised of 10 digits.

        if(this.CPR.matches("\\d{10}")) {
            return true;
        }
        return false;
    }
}
