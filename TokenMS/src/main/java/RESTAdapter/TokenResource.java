package RESTAdapter;

import RESTAdapter.DTO.TokenRequestDTO;
import MessageAdapter.IMessageSender;
import MessageAdapter.MessageSender;
import TokenCore.*;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import javax.imageio.ImageIO;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.*;
import java.net.InetAddress;

import java.util.HashSet;

@Path("/tokens")
public class TokenResource {
    private TokenManager tokenManager;

    public TokenResource(TokenManager tokenManager) {
        this.tokenManager = tokenManager;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/request")
    public Response requestTokens(TokenRequestDTO tokenRequest) throws IOException, InterruptedException {
        System.out.println("hohooh");
        String CPR = tokenRequest.get_CPR();
        int numberOfTokens = tokenRequest.get_numberOfTokens();

        HashSet<Token> tokenSet ;

        boolean reply;
        try {
            System.out.println("Customer exists");
            tokenSet = tokenManager.requestTokens(CPR, numberOfTokens);
        }
        catch (InvalidTokenException e) {
            return Response.status(422).entity(e.getMessage()).build();
        }

        JsonArray urls = new JsonArray();

        if(tokenSet.size() > 0) {

            InetAddress inetAddress = InetAddress.getLocalHost();
            String ipAddress = inetAddress.getHostAddress();

            for (Token token : tokenSet) {

                //This port will be used in the final version
                String port = "8082";

                JsonObject bar_url = new JsonObject();
                bar_url.addProperty("Bar Code", token.getBarCode().getCode());
                bar_url.addProperty("URL", "http://" + ipAddress + ":" + port + "/tokens/barcode/" + token.getBarCode().getCode());

                urls.add(bar_url);
            }
        }

        return Response.ok().entity(urls.toString()).build();
    }

    @GET
    @Produces("image/png")
    @Path("/barcode/{barcode}")
    public Response getBarCode(@PathParam("barcode") String barCode) throws IOException {
        Token token;

        try {
            token = tokenManager.repo.getToken(barCode);

        } catch (InvalidTokenException e) {

            return Response.status(400).entity(e.getMessage()).build();
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        ImageIO.write(token.getBufferedImage(), "png", baos);
        byte[] imageData = baos.toByteArray();

        //return Response.ok(imageData).build();
        return Response.ok(new ByteArrayInputStream(imageData)).build();

    }

}
