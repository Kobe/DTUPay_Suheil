package RESTAdapter;

import PaymentCore.InvalidToken;
import PaymentCore.Payment;
import PaymentCore.PaymentManager;
import bank.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/payment")
public class PaymentResource {

    private PaymentManager paymentManager;

    public PaymentResource(PaymentManager paymentManager) {
        this.paymentManager = paymentManager;
    }

    @Path("/transfer")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response TransferMoney(Payment payment) throws InvalidToken {
        try {

            paymentManager.MakeTransaction(payment);
            return Response.status(Response.Status.OK).entity("Transaction went successfully").build();

        } catch (BankServiceException_Exception e)
        {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getFaultInfo().getErrorMessage()).build();
        }

    }

}
