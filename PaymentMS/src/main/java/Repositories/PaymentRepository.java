package Repositories;

import PaymentCore.Payment;
import Repositories.IPaymentRepository;

import java.util.HashMap;

public class PaymentRepository implements IPaymentRepository {

    private HashMap<String, Payment> _payments;

    public PaymentRepository() {
        this._payments = new HashMap<>();
    }

    @Override
    public boolean add(Payment p) {

        //No token found
        if (p.getTokenId() == null || p.getTokenId().isEmpty()){
            System.out.println("NO token found");
            return false;
        }

        //Token already used
        if(_payments.containsKey(p.getTokenId())) {
            System.out.println("Token already used");
            return false;
        }

        //Add payment
        _payments.put(p.getTokenId(), p);
        return true;
    }

    @Override
    public boolean remove(Payment p) {
        return _payments.remove(p.getTokenId(), p);
    }
}
