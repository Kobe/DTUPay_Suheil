package ICustomerRepositories;

import CustomerCore.InvalidCPRException;
import CustomerCore.Customer;

public interface ICustomerRepository {

    boolean add(Customer c);

    boolean remove(Customer c);

    Customer getCustomerByCPR(String CPR) throws InvalidCPRException;


}
