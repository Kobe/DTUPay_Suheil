Feature: Transcation
  Scenario: Make a transaction
    When a customer transfers 50 kroners to a merchant
    Then the merchant should have the balance increased by 50 kroners.