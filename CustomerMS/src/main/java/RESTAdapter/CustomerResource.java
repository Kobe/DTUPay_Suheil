package RESTAdapter;


import CustomerCore.*;
import com.google.gson.Gson;
import org.wildfly.swarm.spi.runtime.annotations.Post;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/customer")
public class CustomerResource {

    private CustomerManager customerManager;

    public CustomerResource(CustomerManager customerManager){
        this.customerManager = customerManager;
    }

    @Path("/register")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerCustomer(Customer customer) {
        if(!customer.isCPRValid()) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity(new Gson().toJson("Invalid CPR")).build();
        }

        if (customerManager.RegisterCustomer(customer))
        {
            return Response.ok(new Gson().toJson(customer)).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity(new Gson().toJson("Could not add the customer")).build();
    }

    @Path("/refund/{CPR}")
    @Post
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response refundCustomer(@PathParam("CPR") String CPR){
        //TODO - make message qeue towards merchant microservice to

        return Response.status(200).build();
    }
}

