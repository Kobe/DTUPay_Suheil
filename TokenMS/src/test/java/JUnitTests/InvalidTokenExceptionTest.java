package JUnitTests;

import TokenCore.InvalidTokenException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class InvalidTokenExceptionTest {

    InvalidTokenException tokenException;

    @Before
    public void setUp() throws Exception {
        tokenException  = new InvalidTokenException("test");
    }

    @Test
    public void getMessage() {
        assertEquals("test", tokenException.getMessage());
    }


}