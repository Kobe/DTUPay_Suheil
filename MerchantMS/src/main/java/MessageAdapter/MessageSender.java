package MessageAdapter;

import MerchantCore.Merchant;
import MessageAdapter.DTO.PaymentRequestDTO;
import RESTAdapter.DTO.TransferRequestDTO;
import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;

public class MessageSender implements IMessageSender {
    private Connection connection;
    private Channel channel;

    private String QUEUE_CHECK_TOKEN = "checkToken";
    private String QUEUE_REQUEST_PAYMENT = "requestPayment";

    public MessageSender() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
        System.out.println("Sender instantiated");
        connection = factory.newConnection();
        channel = connection.createChannel();
    }

    private String call(String queueName, String message) throws IOException, InterruptedException {
        final String corrId = UUID.randomUUID().toString();
        System.out.println("Sending request: " + message);

        String replyQueueName = channel.queueDeclare().getQueue();
        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();

        channel.basicPublish("", queueName, props, message.getBytes("UTF-8"));

        final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);

        String ctag = channel.basicConsume(replyQueueName, true, (consumerTag, delivery) -> {
            if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                response.offer(new String(delivery.getBody(), "UTF-8"));
            }
        }, consumerTag -> {
        });

        String result = response.take();
        System.out.println("Received message: " + result);
        channel.basicCancel(ctag);
        return result;
    }

    @Override
    public String callCheckToken(String tokenID) throws IOException, InterruptedException {
        return call(QUEUE_CHECK_TOKEN, tokenID);
    }

    @Override
    public boolean callRequestPayment(int amount, String fromAccount, String toBankAccount, String description, String tokenID) {
        System.out.println("Calling requestpayment");
        PaymentRequestDTO dto = new PaymentRequestDTO(fromAccount, toBankAccount, amount, description, tokenID);
        try {
            String result = call(QUEUE_REQUEST_PAYMENT, (new Gson()).toJson(dto));
            System.out.println("result " + result);
            if ("PaymentSuccess".equals(result)) {
                System.out.println("Payment success!");
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean callAddMerchant(Merchant m) {
        return false;
    }

    @Override
    public boolean callRemoveMerchant(Merchant m) {
        return false;
    }

    @Override
    public Merchant callGetMerchantByID(String ID) {
        return null;
    }
}
