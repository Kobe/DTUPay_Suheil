package CustomerCore;

import MessageAdapter.MessageReceiver;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by karol on 22.1.2019.
 */
public class CustomerManagerFactory implements ICustomerManagerFactory {
    private static CustomerManagerFactory customerManagerFactory = null;
    private CustomerManager customerManager;

    private CustomerManagerFactory() {
        System.out.println("Instantiating manager");
        this.customerManager = new CustomerManager(new CustomerRepository());
    }

    public static CustomerManagerFactory getInstance() {
        System.out.println("Getting factory");
        if (customerManagerFactory == null) {
            System.out.println("ready");
            customerManagerFactory = new CustomerManagerFactory();
        }

        return customerManagerFactory;
    }

    @Override
    public CustomerManager create() {
        return this.customerManager;
    }
}
