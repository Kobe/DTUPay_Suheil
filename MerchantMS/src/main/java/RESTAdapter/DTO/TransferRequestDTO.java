package RESTAdapter.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TransferRequestDTO {
    @JsonProperty("merchantID")
    private String merchantID;
    @JsonProperty("tokenID")
    private String tokenID;
    @JsonProperty("amount")
    private int amount;
    @JsonProperty("message")
    private String message;

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getTokenID() {
        return tokenID;
    }

    public void setTokenID(String tokenID) {
        this.tokenID = tokenID;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
