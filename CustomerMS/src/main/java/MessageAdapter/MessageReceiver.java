package MessageAdapter;

import CustomerCore.CustomerManager;
import CustomerCore.InvalidCPRException;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

public class MessageReceiver {
    private String QUEUE_CHECK_CUSTOMER = "checkCustomer";
    private CustomerManager customerManager;
    private Channel channel;

    public MessageReceiver(CustomerManager customerManager) throws IOException, TimeoutException {
        this.customerManager = customerManager;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
        Connection connection = factory.newConnection();
        this.channel = connection.createChannel();
    }

    public void initialize() throws IOException {
        initializeQueue(QUEUE_CHECK_CUSTOMER, this::callCheckCustomer);
    }

    private void initializeQueue(String queueName, Function<String, String> func) throws IOException {
        channel.queueDeclare(queueName, false, false, false, null);
        System.out.println(" [*] Waiting for messages on queue '" + queueName + "'.");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "' on queue '" + queueName +"'");

            AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                    .Builder()
                    .correlationId(delivery.getProperties().getCorrelationId())
                    .build();

            String response = func.apply(message);
            channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, response.getBytes("UTF-8"));
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            System.out.println("Sent reply for message received on queue '" + queueName + "'.");

        };
        channel.basicConsume(queueName, false, deliverCallback, consumerTag -> { });
    }

    public String callCheckCustomer(String CPR) {
        // Parse message and return some response
        try {
            if(this.customerManager.RetreiveCustomer(CPR) != null){
                return "" + true;
            }

            return "" + false;
        } catch (InvalidCPRException e) {
            e.printStackTrace();
            return "" + false;
        }
    }
}
