package MessageAdapter;

import MerchantCore.Merchant;

import java.io.IOException;

public interface IMessageSender {
    String callCheckToken(String tokenId) throws IOException, InterruptedException;
    boolean callRequestPayment(int amount, String fromAccount, String toBankAccount, String description, String tokenID);
    boolean callAddMerchant(Merchant m);
    boolean callRemoveMerchant(Merchant m);
    Merchant callGetMerchantByID(String ID);
}
