package CustomerCore;

import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerTest {

    private Customer c =  new Customer();
    private String fullName = "Alice nBob";
    private String firstName = "Alice";
    private String lastName = "nBob";
    private String cpr = "123456789";

    @Test
    public void getFullName() {
        c.setFirstName(firstName);
        c.setLastName(lastName);
        assertEquals(fullName, c.getFullName());
    }

    @Test
    public void getLastName() {
        c.setLastName(lastName);
        assertEquals(lastName, c.getLastName());
    }

    @Test
    public void setLastName() {
        c.setLastName("Test");
        assertEquals("Test", c.getLastName());
    }

    @Test
    public void getFirstName() {
        c.setFirstName(firstName);
        assertEquals(firstName, c.getFirstName());
    }

    @Test
    public void getCPR() throws InvalidCPRException {
        c.setCPR(cpr);
        assertEquals(cpr, c.getCPR());
    }

    @Test
    public void setFirstName() {
        c.setFirstName("Test2");
        assertEquals("Test2", c.getFirstName());
    }

    @Test
    public void setCPR() throws InvalidCPRException {
        c.setCPR("newcpr1234");
        assertEquals("newcpr1234", c.getCPR());
    }

    @Test
    public void isCPRValid() throws InvalidCPRException {
        c.setCPR("invalid cpr");
        assertFalse(c.isCPRValid());
    }
}