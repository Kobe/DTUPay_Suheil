package MessageAdapter;

import java.io.IOException;

/**
 * Created by karol on 22.1.2019.
 */
public interface IMessageSender {
    public boolean callCheckCustomer(String CPR) throws IOException, InterruptedException;
}
