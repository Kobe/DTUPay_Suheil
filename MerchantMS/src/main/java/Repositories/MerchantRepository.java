package Repositories;

import MerchantCore.Merchant;

import java.util.HashMap;

public class MerchantRepository implements IMerchantRepository {

    private HashMap<String, String> _merchants;


    public MerchantRepository(){
        _merchants = new HashMap<String, String>();
    }

    @Override
    public boolean add(Merchant m) {
        try{
            _merchants.put(m.getID(), m.getFullName());
            return true;
        }
        catch (Exception e)
        {
            //User already exists
            return false;
        }
    }

    @Override
    public boolean remove(Merchant m) {
        return _merchants.remove(m.getID(), m.getFullName());

    }

    @Override
    public Merchant getMerchantByID(String CPR) {
        String fullName = _merchants.get(CPR);

        if (fullName == null || fullName.isEmpty()) return null;

        String[] names = fullName.split(" ");
        String firstName = "", lastName = "";

        boolean first = true;
        for (String n: names){
            if (first)
            {
                first = !first;
                firstName = n;
                continue;
            }
            lastName += n + "";
        }
        return new Merchant(CPR,firstName,lastName);

    }

}

