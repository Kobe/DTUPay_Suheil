package CustomerCore;

public class InvalidCPRException extends Throwable {
    public InvalidCPRException(String message) {
        super(message);
    }
}
