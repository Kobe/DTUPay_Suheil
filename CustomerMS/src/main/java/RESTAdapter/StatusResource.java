package RESTAdapter;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

@Path("/status")
public class StatusResource {
    @GET
    @Produces("text/plain")
    public Response doGet() {
        return Response.ok("Customer service is up.").build();
    }
}