package Cucumber;

import PaymentCore.Payment;
import PaymentCore.PaymentManager;
import Repositories.PaymentRepository;
import bank.*;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.math.BigDecimal;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import static junit.framework.TestCase.assertEquals;

public class TransactionSteps {

    private PaymentManager paymentManager;
    private BankService bankService;
    private User customer;
    private User merchant;

    private final int amountToTransfer = 50;
    private int initialAmount = 100;

    @Before
    public void setUp() throws BankServiceException_Exception {
        this.bankService = new BankServiceService().getBankServicePort();
        this.paymentManager = new PaymentManager(new PaymentRepository());


        //Create two users with 100kr balance
        this.customer = new User();
        this.merchant = new User();



        int cpr1 = ThreadLocalRandom.current().nextInt(100000000, 999999999 + 1);
        int cpr2 = ThreadLocalRandom.current().nextInt(100000000, 999999999 + 1);

        customer.setCprNumber(cpr1+""); customer.setFirstName("Alice"); customer.setLastName("Bob");
        merchant.setCprNumber(cpr2+""); merchant.setFirstName("Alice"); merchant.setLastName("Bob");

        bankService.createAccountWithBalance(customer, new BigDecimal(initialAmount));
        bankService.createAccountWithBalance(merchant, new BigDecimal(initialAmount));
    }

    @When("^a customer transfers (\\d+) kroners to a merchant$")
    public void a_customer_transfers_kroners_to_a_merchant(int arg1) throws Throwable {

        //Transfer 50 kroners from customer to merchant
        Payment p = new Payment(customer.getCprNumber(), merchant.getCprNumber(), amountToTransfer, "Cucumber test", "456465456465456465");
        Random rnd = new Random();
        String rndId = Integer.toString(rnd.nextInt(999999999) + 1000000000);
        p.setTokenId("TokenRunFromTest" + rndId);
        paymentManager.MakeTransaction(p);

    }

    @Then("^the merchant should have the balance increased by (\\d+) kroners\\.$")
    public void the_merchant_should_have_the_balance_increased_by_kroners(int arg1) throws Throwable {
        // Check if the merchant has his balance increased by the initialAmount transferred in the previous step

        Account merchantAccount = bankService.getAccountByCprNumber(merchant.getCprNumber());
        Account customerAccount = bankService.getAccountByCprNumber(customer.getCprNumber());

        //Merchant account balance got increased
        assertEquals(initialAmount + amountToTransfer, merchantAccount.getBalance().intValue());

        //Customer account balance got decreased
        assertEquals(initialAmount - amountToTransfer, customerAccount.getBalance().intValue());

    }
}
