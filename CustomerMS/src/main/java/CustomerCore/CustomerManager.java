package CustomerCore;

public class CustomerManager {

    public CustomerRepository repo;

    public CustomerManager(CustomerRepository repo) {
        System.out.println("Instantiating repo");
        this.repo = repo;
    }

    public boolean RegisterCustomer(Customer customer)
    {
        return repo.add(customer);
    }

    public Customer RetreiveCustomer(String cpr) throws InvalidCPRException {
        return repo.getCustomerByCPR(cpr);
    }

}
