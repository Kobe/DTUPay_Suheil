package MessageAdapter;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;

/**
 * Created by karol on 22.1.2019.
 */
public class MessageSender implements IMessageSender {
    private Connection connection;
    private Channel channel;
    private String QUEUE_CHECK_CUSTOMER = "checkCustomer";

    public MessageSender() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
        System.out.println("Sender instantiated");
        connection = factory.newConnection();
        channel = connection.createChannel();
    }


    private String call(String queueName, String message) throws IOException, InterruptedException {
        final String corrId = UUID.randomUUID().toString();
        System.out.println("Sending request: " + message);

        String replyQueueName = channel.queueDeclare().getQueue();
        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();

        channel.basicPublish("", queueName, props, message.getBytes("UTF-8"));

        System.out.println("Sent message...");

        final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);

        System.out.println("Waiting for reply...");
        String ctag = channel.basicConsume(replyQueueName, true, (consumerTag, delivery) -> {
            if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                response.offer(new String(delivery.getBody(), "UTF-8"));
            }
        }, consumerTag -> {
        });

        String result = response.take();
        System.out.println("Received message: " + result);
        channel.basicCancel(ctag);
        return result;
    }

    @Override
    public boolean callCheckCustomer(String CPR) throws IOException, InterruptedException {
        boolean exists = Boolean.parseBoolean(call(this.QUEUE_CHECK_CUSTOMER, CPR));
        return exists;
    }
}
