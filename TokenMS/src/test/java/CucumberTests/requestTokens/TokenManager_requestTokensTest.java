package CucumberTests.requestTokens;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "Features"
                ,tags = {"@CucumberTests.requestTokens"}
                )

public class TokenManager_requestTokensTest {
}
