import java.util.List;

public class TokenList {

    private List<TokenResponse> tokenResponses;

    public TokenList() {

    }

    public void setTokenRespones(List<TokenResponse> tokenResponses) {
        this.tokenResponses = tokenResponses;
    }

    public List<TokenResponse> getTokenReponses() {
        return this.tokenResponses;
    }

}
