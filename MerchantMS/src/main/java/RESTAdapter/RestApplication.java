package RESTAdapter;

import MerchantCore.MerchantManager;
import MessageAdapter.MessageSender;
import Repositories.MerchantRepository;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeoutException;

@ApplicationPath("/")
public class RestApplication extends Application {
    private MerchantManager merchantManager = null;

    public RestApplication() throws IOException, TimeoutException {
        System.out.println("Initializing application...");
        merchantManager = new MerchantManager(new MerchantRepository(), new MessageSender());
    }

    @Override
    public Set<Object> getSingletons() {
        Set<Object> set = new HashSet<>();
        set.add(new StatusResource());
        set.add(new MerchantResource(merchantManager));
        return set;
    }
}
