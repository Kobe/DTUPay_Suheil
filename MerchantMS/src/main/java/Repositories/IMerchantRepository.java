package Repositories;

import MerchantCore.Merchant;

public interface IMerchantRepository {

        boolean add(Merchant m);

        boolean remove(Merchant m);

        Merchant getMerchantByID(String ID);

}
