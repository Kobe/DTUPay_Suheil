Feature: Use Token

    @CucumberTests.useToken
    Scenario: Customer uses 1 token
        Given a customer with 1 token
        When the customer uses the token
        Then the customer has 0 tokens in total