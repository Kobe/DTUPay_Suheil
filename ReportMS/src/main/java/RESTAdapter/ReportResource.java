package RESTAdapter;

import IPaymentServiceReports.IPaymentServiceReports;
import ReportCore.CustomerReport;
import ReportCore.MerchantReport;
import ReportCore.PaymentServiceReportsQueue;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by karol on 19.1.2019.
 */
@Path("/reports")
public class ReportResource {
    private IPaymentServiceReports paymentServiceReportsQueue;

    public ReportResource(){
        paymentServiceReportsQueue = new PaymentServiceReportsQueue();
    }

    @Path("/merchant/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMerchantReports(@PathParam("id") String merchantID, @QueryParam("from")String from, @QueryParam("until") String until){
        DateFormat formatter = new SimpleDateFormat("MM/dd/yy");

        try {
            Date fromDate = formatter.parse(from);
            Date untilDate = formatter.parse(until);

            ArrayList<MerchantReport> reports = new ArrayList<MerchantReport>();
            //TODO - create a message qeue to request reports from payment microservice
            MerchantReport report1 = new MerchantReport(formatter.parse("11/10/1995"), "Some transaction", "123456789", 100);
            MerchantReport report2 = new MerchantReport(formatter.parse("11/11/1995"), "Some transaction", "123456789", 100);

            reports.add(report1);
            reports.add(report2);

            return Response.status(200).entity(new Gson().toJson(reports)).build();
        } catch (ParseException e) {
            e.printStackTrace();
            return Response.status(422).entity(new Gson().toJson("Wrong date format")).build();
        }
    }

    @Path("/customer/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerReports(@PathParam("id") String customerID, @QueryParam("from")String from, @QueryParam("until") String until){
        DateFormat formatter = new SimpleDateFormat("MM/dd/yy");
        try {
            Date fromDate = formatter.parse(from);
            Date untilDate = formatter.parse(until);

            ArrayList<CustomerReport> reports = new ArrayList<CustomerReport>();
            //TODO - create a message qeue to request reports from payment microservice
            CustomerReport report1 = new CustomerReport(formatter.parse("11/10/1995"), "Some transaction", "123456789", 100, "0123");
            CustomerReport report2 = new CustomerReport(formatter.parse("11/11/1995"), "Some transaction", "123456789", 100, "0123");

            reports.add(report1);
            reports.add(report2);

            return Response.status(200).entity(new Gson().toJson(reports)).build();
        } catch (ParseException e) {
            e.printStackTrace();
            return Response.status(422).entity(new Gson().toJson("Wrong date format")).build();
        }
    }
}
