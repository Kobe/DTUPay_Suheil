package RESTAdapter;

import CustomerCore.CustomerManager;
import CustomerCore.CustomerRepository;
import MessageAdapter.MessageReceiver;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/")
public class RestApplication extends Application {
    private CustomerManager customerManager = null;

    public RestApplication() {
        System.out.println("Initializing application...");
        customerManager = new CustomerManager(new CustomerRepository());
        MessageReceiver receiver = null;
        try {
            receiver = new MessageReceiver(customerManager);
            receiver.initialize();
        } catch (Exception e) {
            System.out.println("Failed to initialize message queue.");
            e.printStackTrace();
        }
    }

    @Override
    public Set<Object> getSingletons() {
        Set<Object> set = new HashSet<>();
        set.add(new StatusResource());
        set.add(new CustomerResource(customerManager));
        return set;
    }
}
