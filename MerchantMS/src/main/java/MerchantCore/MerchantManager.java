package MerchantCore;

import Repositories.IMerchantRepository;
import MessageAdapter.IMessageSender;
import RESTAdapter.DTO.TransferRequestDTO;

import java.io.IOException;

public class MerchantManager {
    private IMessageSender messageSender;

    public IMerchantRepository repo;

    public MerchantManager(IMerchantRepository repo, IMessageSender messageSender) {
        this.repo = repo;
        this.messageSender = messageSender;
    }

    public boolean transfer(TransferRequestDTO dto) {
        if (repo.getMerchantByID(dto.getMerchantID()) == null) {
            System.out.printf("Error: No merchant with id '" + dto.getMerchantID() + "' found.");
            return false;
        }
        try {
            // Check if token is not used before
            String userCPR = messageSender.callCheckToken(dto.getTokenID());
            boolean tokenValid = !userCPR.equals("");
            if (tokenValid) {
                System.out.println("token valid");
                boolean paymentSuccess = messageSender
                        .callRequestPayment(dto.getAmount(), userCPR, dto.getMerchantID(), dto.getMessage(), dto.getTokenID());
                System.out.println("Payment succeded: " + paymentSuccess);
                return paymentSuccess;
            } else {
                System.out.println("token invalid");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
