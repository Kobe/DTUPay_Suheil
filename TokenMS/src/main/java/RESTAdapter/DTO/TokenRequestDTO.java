package RESTAdapter.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by karol on 19.1.2019.
 * According to my research this is the simplest method to get request
 */
public class TokenRequestDTO {
    @JsonProperty("_numberOfTokens")
    private int _numberOfTokens;
    @JsonProperty("_CPR")
    private String _CPR;

    public TokenRequestDTO(){

    }

    public String get_CPR() {
        return this._CPR;
    }

    public void set_CPR(String _CPR) {
        this._CPR = _CPR;
    }

    public int get_numberOfTokens() {
        return _numberOfTokens;
    }

    public void set_numberOfTokens(int _numberOfTokens) {
        this._numberOfTokens = _numberOfTokens;
    }
}
