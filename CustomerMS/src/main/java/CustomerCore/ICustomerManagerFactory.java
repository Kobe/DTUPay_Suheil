package CustomerCore;

/**
 * Created by karol on 22.1.2019.
 */
public interface ICustomerManagerFactory {
    public CustomerManager create();
}
