import com.fasterxml.jackson.annotation.JsonProperty;

public class TokenResponse {

    @JsonProperty("Bar Code")
    private String _barCode;

    @JsonProperty("URL")
    private String _URL;

    public TokenResponse() {

    }

    public void setBarCode(String barCode) {this._barCode = barCode; }

    public void setURL(String URL) { this._URL = URL; }

    public String getBarCode() { return this._barCode; }

    public String getURL() { return this._URL; }
}
