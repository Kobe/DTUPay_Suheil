package Cucumber;

import CustomerCore.Customer;
import CustomerCore.CustomerManager;
import CustomerCore.CustomerRepository;
import CustomerCore.InvalidCPRException;
import MessageAdapter.MessageReceiver;
import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import static junit.framework.TestCase.*;

public class CustomerSteps {

    private boolean customerExist;
    private Customer c = new Customer("Alice", "Bob", "123456789");
    private CustomerManager cm = new CustomerManager(new CustomerRepository());

    public CustomerSteps() throws InvalidCPRException{
    }

    @When("^a customer does not exist$")
    public void a_customer_does_not_exist() throws Throwable {

        customerExist = cm.RetreiveCustomer(c.getCPR()) != null;
        assertFalse(customerExist);

    }

    @Then("^it most be possible to create a new customer in the system$")
    public void it_most_be_possible_to_create_a_new_customer_in_the_system() throws Throwable {

        cm.RegisterCustomer(c);
        customerExist = cm.RetreiveCustomer(c.getCPR()) != null;
        assertTrue(customerExist);

    }
}
