import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {

    private Main m;

    @Before
    public void setUp() throws Exception {
        m = new Main();
    }

    @Test
    public void getHelloSystemTest() {
        assertEquals(m.getHelloSystemTest(), "Hello from SystemTest");
    }
}