package CustomerCore;

import ICustomerRepositories.ICustomerRepository;

import java.util.HashMap;

public class CustomerRepository implements ICustomerRepository {

    private HashMap<String, String> _customers;

    public CustomerRepository(){
        _customers = new HashMap<String, String>();
    }

    @Override
    public boolean add(Customer c) {

        if(_customers.containsKey(c.getCPR())) {
            return false;
        }
        _customers.put(c.getCPR(), c.getFullName());
        return true;
    }

    @Override
    public boolean remove(Customer c) {
        return _customers.remove(c.getCPR(), c.getFullName());

    }

    @Override
    public Customer getCustomerByCPR(String CPR) throws InvalidCPRException {

        String name = _customers.get(CPR);

        if (name == null || name.isEmpty()) return null;

        String[] names = name.split(" ");
        String firstName = "", lastName = "";

        boolean first = true;
        for (String n: names){
            if (first)
            {
                first = !first;
                firstName = n;
                continue;
            }
            lastName += n + "";
        }
        return new Customer(firstName,lastName,CPR);
    }




}
