package ReportCore;

import java.util.Date;

/**
 * Created by karol on 19.1.2019.
 */
public class CustomerReport {
    private Date date;
    private String note;
    private String transactionID;
    private int amount;
    private String merchantID;

    public CustomerReport(Date date, String note, String transactionID, int amount, String merchantID) {
        this.date = date;
        this.note = note;
        this.transactionID = transactionID;
        this.amount = amount;
        this.merchantID = merchantID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }
}
