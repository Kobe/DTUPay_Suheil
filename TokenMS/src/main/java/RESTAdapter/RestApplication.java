package RESTAdapter;

import MessageAdapter.MessageReceiver;
import MessageAdapter.MessageSender;
import TokenCore.TokenManager;
import Repositories.TokenRepository;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeoutException;

@ApplicationPath("/")
public class RestApplication extends Application {
    private TokenManager tokenManager = null;

    public RestApplication() throws IOException, TimeoutException {
        System.out.println("Initializing application...");
        tokenManager = new TokenManager(new TokenRepository(), new MessageSender());
        MessageReceiver receiver = null;
        try {
            receiver = new MessageReceiver(tokenManager);
            receiver.initialize();
        } catch (Exception e) {
            System.out.println("Failed to initialize message queue.");
            e.printStackTrace();
        }
    }

    @Override
    public Set<Object> getSingletons() {
        Set<Object> set = new HashSet<>();
        set.add(new StatusResource());
        set.add(new TokenResource(tokenManager));
        return set;
    }
}

