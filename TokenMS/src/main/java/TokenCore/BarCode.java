package TokenCore;

import org.krysalis.barcode4j.impl.upcean.EAN13Bean;
import org.krysalis.barcode4j.impl.upcean.UPCEANLogicImpl;

import java.util.Random;

public class BarCode {

private EAN13Bean _bean;
    // Suheil
    private String code;

    public BarCode() {
        this.code = genEAN13Code();
    }

    public String getCode() {
        return this.code;
    }

    private String genEAN13Code() {

        Random rand = new Random();
        _bean = new EAN13Bean();
        UPCEANLogicImpl impl = _bean.createLogicImpl();

        String code = "";

        for (int i = 0; i < 12; i++) {
            code += Integer.toString(rand.nextInt(9) + 1);
        }

        code += impl.calcChecksum(code);

        return code;
    }

    public EAN13Bean getBean() {
        return this._bean;
    }
}
