#!/usr/bin/env sh
scriptdir="$(dirname "$0")"
cd "$scriptdir"
set -e
GREEN="\033[1;32m"
NOCOLOR="\033[0m"

log_console()
{
    echo -e "${GREEN}$1${NOCOLOR}"
}

log_console "Building tokenms ('mvn package')...\n"
mvn package

pushd "../"
log_console "\nBuilding Docker image and starting container ('docker-compose up -d --build tokenms')...\n"
docker-compose up -d --build tokenms
popd

log_console "\nContainer started. It might take a while to initialize"