package PaymentCore;

import Repositories.PaymentRepository;
import bank.*;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.*;

public class PaymentManagerTest {

    BankService bankService = new BankServiceService().getBankServicePort();
    PaymentManager paymentManager = new PaymentManager(new PaymentRepository());

    @Test
    public void makeTransaction() throws BankServiceException_Exception, InvalidToken {
        //Create two users with balance
        User c = new User();
        User m = new User();
        int amount = 100;
        int transfer = 50;


        int cpr1 = ThreadLocalRandom.current().nextInt(100000000, 999999999 + 1);
        int cpr2 = ThreadLocalRandom.current().nextInt(100000000, 999999999 + 1);

        c.setCprNumber(cpr1+""); c.setFirstName("Alice"); c.setLastName("Bob");
        m.setCprNumber(cpr2+""); m.setFirstName("Alice"); m.setLastName("Bob");

        bankService.createAccountWithBalance(c, new BigDecimal(amount));
        bankService.createAccountWithBalance(m, new BigDecimal(amount));


        Payment payment = new Payment(c.getCprNumber(), m.getCprNumber(), transfer, "Test", "45646545646");
        paymentManager.MakeTransaction(payment);


        //c should now have been transfered 50 kroners to m
        Account userC = bankService.getAccountByCprNumber(c.getCprNumber());
        Account userM = bankService.getAccountByCprNumber(m.getCprNumber());

        assertEquals(amount+transfer, userM.getBalance().intValue());
        assertEquals(amount-transfer, userC.getBalance().intValue());

    }
}