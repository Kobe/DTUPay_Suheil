package MerchantCore;

public class Merchant {

    private String ID, firstName, lastName;

    public Merchant(){}

    public Merchant(String ID, String firstName, String lastName){
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFullName()
    {
        return this.firstName + " " + this.lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getID() {
        return this.ID;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
}
