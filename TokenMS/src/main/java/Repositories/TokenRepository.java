package Repositories;

import TokenCore.InvalidTokenException;
import TokenCore.Token;

import java.util.*;

/**
 * Created by karol on 18.1.2019.
 */
public class TokenRepository implements ITokenRepository {

    private LinkedHashMap<String, HashSet<Token>> _tokenOwners;
    private LinkedHashMap<Token, String> _allTokens;

    public TokenRepository(){
        this._tokenOwners = new LinkedHashMap<String, HashSet<Token>>();
        this._allTokens  = new LinkedHashMap<Token, String>();
    }


    @Override
    public boolean addTokens(Token token, String CPR) {

        if(!this._tokenOwners.containsKey(CPR)) {

            // if the customer has not requested tokens before.
            this._tokenOwners.put(CPR, new HashSet<Token>());
        }

        // add the token to the customer hashset
        this._tokenOwners.get(CPR).add(token);

        // add the token to the list of all tokens
        this._allTokens.put(token, CPR);

        return true;
    }

    //retrieves Token
    @Override
    public HashSet<Token> getCustomerTokens(String CPR) {

        return (HashSet<Token>) this._tokenOwners.get(CPR);
    }

    @Override
    public Token getToken(String barCode) throws InvalidTokenException {
        if(tokenExists(barCode)) {
            for (Token token : _allTokens.keySet()) {
                if (token.getBarCode().getCode().equals(barCode)) {
                    return token;
                }
            }
        } else {
            throw new InvalidTokenException("No Token Found");

        }
        return null;
    }


    //invalidates token
    @Override
    public boolean invalidateToken(String barCode) {

        // Iterate over all tokens
        for (Token t : _allTokens.keySet()) {

            // if the barCode matches the barCode within the token
            if(t.getBarCode().getCode().equals(barCode)) {

                t.setUsed();

                // Get the CPR associated with this token
                String CPR = _allTokens.get(t);

                // Remove the token from _tokenOwners.
                _tokenOwners.get(CPR).remove(t);

                return true;
            }
        }

        return false;

    }

    // checks if token exists
    @Override
    public boolean tokenExists(String barCode) {

        for (Token t : _allTokens.keySet()) {
            if(t.getBarCode().getCode().equals(barCode)) return true;
        }

        return false;
    }

    public String getCustomer(String barCode) {

        String CPR = null;
        if(tokenExists(barCode)) {
            for (Token token : _allTokens.keySet()) {
                if(token.getBarCode().getCode().equals(barCode)) {
                    if(!token.isUsed()) {
                        CPR =  _allTokens.get(token);
                    }
                }
            }
        }

        return CPR;
    }

    // checks if token is valid/unused
    @Override
    public boolean isTokenValid(String barCode) {
        boolean state = false;

        for (Token t : _allTokens.keySet()) {
            if(t.getBarCode().getCode().equals(barCode)) {
                state = !t.isUsed();
            }
        }
        return state;
    }

    @Override
    public boolean clearDatabase() {
        this._tokenOwners = new LinkedHashMap<String, HashSet<Token>>();
        this._allTokens  = new LinkedHashMap<Token, String>();
        return true;
    }
}
