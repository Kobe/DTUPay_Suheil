package RESTAdapter;


import MerchantCore.*;
import RESTAdapter.DTO.TransferRequestDTO;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

@Path("/merchant")
public class MerchantResource {
    private MerchantManager merchantManager;

    public MerchantResource(MerchantManager merchantManager)  {
        this.merchantManager = merchantManager;
    }

    @Path("/register/")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerMerchant(Merchant merchant) {
        Merchant m = merchant;

        if (merchantManager.repo.add(merchant))
        {
            return Response.ok(new Gson().toJson("Merchant created!")).build();
        }

        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @Path("/transfer")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response transfer(TransferRequestDTO dto) {

        boolean success = merchantManager.transfer(dto);
        if (success) {
            return Response.status(Response.Status.OK).entity(new Gson().toJson("Payment Successful")).build();
        }
        return Response.status(Response.Status.NOT_ACCEPTABLE).entity(new Gson().toJson("Payment Failed")).build();
    }

}

