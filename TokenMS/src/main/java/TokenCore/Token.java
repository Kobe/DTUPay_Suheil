package TokenCore;

import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created by karol on 18.1.2019.
 */
public class Token {
    private boolean _isUsed;
    private String _url;
    private String Barcode;
    private BarCode _barCode;
    private BufferedImage _image;

    public Token() throws IOException {
        this._barCode = new BarCode();
        this._isUsed = false;
        this._url = "";
        this._image = genBarCodeBufferedImage(320);
    }

    public boolean isUsed() {
        return _isUsed;
    }

    public void setUsed() {
        _isUsed = true;
    }

    public String getUrl() {
        return _url;
    }

    public void setUrl(String url) {
        this._url = url;
    }

    public BarCode getBarCode() {
        return this._barCode;
    }

    public BufferedImage getBufferedImage() {
        return this._image;
    }

    private BufferedImage genBarCodeBufferedImage(int dpi) throws IOException {

        BitmapCanvasProvider provider = new BitmapCanvasProvider(
                dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);

        this._barCode.getBean().generateBarcode(provider, this._barCode.getCode());

        provider.finish();

        return provider.getBufferedImage();
    }

}
