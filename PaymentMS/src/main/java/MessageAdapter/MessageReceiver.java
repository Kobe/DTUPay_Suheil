package MessageAdapter;

import MessageAdapter.DTO.PaymentRequestDTO;
import PaymentCore.InvalidToken;
import PaymentCore.Payment;
import PaymentCore.PaymentManager;
import bank.BankServiceException_Exception;
import com.google.gson.Gson;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

public class MessageReceiver {
    private String QUEUE_REQUEST_PAYMENT = "requestPayment";
    private PaymentManager paymentManager;
    private Channel channel;

    public MessageReceiver(PaymentManager paymentManager) throws IOException, TimeoutException {
        this.paymentManager = paymentManager;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
        Connection connection = factory.newConnection();
        this.channel = connection.createChannel();
    }

    public void initialize() throws IOException {
        initializeQueue(QUEUE_REQUEST_PAYMENT, this::requestPayment);
    }

    private void initializeQueue(String queueName, Function<String, String> func) throws IOException {
        channel.queueDeclare(queueName, false, false, false, null);
        System.out.println(" [*] Waiting for messages on queue '" + queueName + "'.");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "' on queue '" + queueName +"'");

            AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                    .Builder()
                    .correlationId(delivery.getProperties().getCorrelationId())
                    .build();

            String response = func.apply(message);
            channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, response.getBytes("UTF-8"));
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            System.out.println("Sent reply for message received on queue '" + queueName + "'.");

        };
        channel.basicConsume(queueName, false, deliverCallback, consumerTag -> { });
    }

    public String requestPayment(String arg1) {
        Gson gson = new Gson();
        Payment dto = gson.fromJson(arg1, Payment.class);
        try {
            paymentManager.MakeTransaction(dto);
            return "PaymentSuccess";
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
            return "BankError";
        } catch (InvalidToken invalidToken) {
            invalidToken.printStackTrace();
            return "InvalidToken";
        }
    }
}
