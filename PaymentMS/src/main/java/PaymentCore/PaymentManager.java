package PaymentCore;

import Repositories.IPaymentRepository;
import Repositories.PaymentRepository;
import bank.Account;
import bank.BankService;
import bank.BankServiceException_Exception;
import bank.BankServiceService;

import java.math.BigDecimal;

public class PaymentManager {

    private BankService bankService;
    private IPaymentRepository _repo;

    public PaymentManager(PaymentRepository repository) {

        this.bankService = new BankServiceService().getBankServicePort();
        this._repo = repository;
    }

    public void MakeTransaction(Payment payment) throws BankServiceException_Exception, InvalidToken {

        if (_repo.add(payment))
        {
            Account from = bankService.getAccountByCprNumber(payment.getFrom());
            Account to = bankService.getAccountByCprNumber(payment.getTo());


            String debtor = from.getId();
            String creditor = to.getId();

            bankService.transferMoneyFromTo(debtor, creditor, new BigDecimal(payment.getAmount()), payment.getDescription());
        }
        else{
            throw new InvalidToken("Token is already used.");
        }



    }
}
