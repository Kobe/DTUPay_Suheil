package Repositories;


import PaymentCore.Payment;

public interface IPaymentRepository {

    boolean add(Payment p);

    boolean remove(Payment p);

}
