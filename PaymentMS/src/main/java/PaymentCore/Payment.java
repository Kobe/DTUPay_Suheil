package PaymentCore;


public class Payment {


    private String from;
    private String to;
    private int amount;
    private String description;
    private String tokenId;

    public Payment() {
    }

    public Payment(String from, String to, int amount, String description, String tokenId)
    {
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.description = description;
        this.tokenId = tokenId;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public int getAmount() {
        return amount;
    }

    public String getDescription() {
        return description;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }






}
