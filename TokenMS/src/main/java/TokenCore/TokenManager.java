package TokenCore;

import MessageAdapter.IMessageSender;
import Repositories.TokenRepository;

import java.io.IOException;
import java.util.HashSet;

/**
 * Created by karol on 18.1.2019.
 */

public class TokenManager {
    public TokenRepository repo;
    private IMessageSender messageSender;

    public TokenManager(TokenRepository repo, IMessageSender messageSender) {
        this.repo = repo;
        this.messageSender = messageSender;
    }

    public HashSet<Token> requestTokens(String CPR, int numberOfTokens) throws InvalidTokenException, IOException, InterruptedException {

        if (!isCustomerRegistered(CPR)) {
            throw new InvalidTokenException("Customer is not registered");
        }

        HashSet<Token> customerTokens = repo.getCustomerTokens(CPR);

        if (numberOfTokens < 1 || numberOfTokens > 5) {
            throw new InvalidTokenException("Invalid Number of Requested Tokens");
        } else if (customerTokens == null) {

            for (int i = 0; i < numberOfTokens; i++) {

                repo.addTokens(genToken(), CPR);
            }

        } else if (customerTokens.size() != 1 || (customerTokens.size() + numberOfTokens) > 6) {
            throw new InvalidTokenException("Number of Unused Tokens is Not Zero");
        } else {
            for (int i = 0; i < numberOfTokens; i++) {
                repo.addTokens(genToken(), CPR);
            }
        }

        return repo.getCustomerTokens(CPR);
    }

    // Returns user cpr if successful, or none if token was already used or nonexisting
    public String useToken(String barCode) {
        String userID = "";
        if (repo.tokenExists(barCode)) {
            if (repo.isTokenValid(barCode)) {
                userID = repo.getCustomer(barCode);
                repo.invalidateToken(barCode);
            }
        }
        System.out.println("returning userID:" + userID);
        return userID;
    }

    public Token genToken() throws IOException {
        Token t = new Token();

        // As long as the repo contains the token, generate another one.
        while (repo.tokenExists(t.getBarCode().getCode())) {
            t = new Token();
        }
        return t;
    }

    private boolean isCustomerRegistered(String CPR) throws IOException, InterruptedException {
        return messageSender.callCheckCustomer(CPR);
    }
}
